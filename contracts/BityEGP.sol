// SPDX-License-Identifier: MIT

pragma solidity =0.6.8;

import "../node_modules/@openzeppelin/contracts-ethereum-package/contracts/token/ERC777/ERC777.sol";
import "../node_modules/@openzeppelin/contracts-ethereum-package/contracts/access/AccessControl.sol";
import "../node_modules/@openzeppelin/contracts-ethereum-package/contracts/utils/Pausable.sol";
import "../node_modules/@openzeppelin/contracts-ethereum-package/contracts/utils/ReentrancyGuard.sol";



contract BityEGP is Initializable, ERC777UpgradeSafe, PausableUpgradeSafe, AccessControlUpgradeSafe, ReentrancyGuardUpgradeSafe {
    bytes32 public constant USER_ROLE = keccak256("USER_ROLE");
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");

    function initialize(address[] memory defaultOperators) public initializer {
        __ERC777_init('BityEGP', 'bEGP', defaultOperators);
        __AccessControl_init();
        __Pausable_init();
        __ReentrancyGuard_init();
        __initRoles();
    }


    function __initRoles() internal {
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());

        _setRoleAdmin(USER_ROLE, DEFAULT_ADMIN_ROLE);
        
        _setupRole(MINTER_ROLE, _msgSender());
        _setupRole(PAUSER_ROLE, _msgSender());
    }



    /**
     * Features
     */

    // Mint tokens into user account
    function deposite(address to, uint256 amount) external {
        require(isOperatorFor(msg.sender, to), "Unauthorized token operator!");
        require(amount > 0, "No tokens to add!");

       _mint(to, amount * 100, "", "");
    }

    // Burn tokens from user account & collect the withdrawal fee
    function Withdrawal(address from, uint256 amount, uint256 fee) external nonReentrant {
        require(isOperatorFor(msg.sender, from), "Unauthorized token operator!");
        require(amount > 0, "No tokens to Withdrawal!");

        uint256 total = amount.add(fee);
        balanceOf(from).sub(total, "BityEGP: withdrawal amount + fee exceed balance");

        if (fee > 0) {
            operatorSend(from, msg.sender, fee, "", ""); 
        }

        operatorBurn(from, amount, "", "");
    }

    // Burn tokens between users accounts & collect the transfer fee
    function transfer(address from, address to, uint256 amount, uint256 fee) external nonReentrant {
        require(isOperatorFor(msg.sender, from), "Unauthorized token operator!");
        require(amount > 0, "No tokens to transfer!");

        uint256 total = amount.add(fee);
        balanceOf(from).sub(total, "BityEGP: transfer amount + fee exceed balance");

        if (fee > 0) {
            operatorSend(from, msg.sender, fee, "", "");
        }

        operatorSend(from, to, amount, "", "");
    }



    /**
     * Pausable  
     */

    // Pause the contract.
    function pause() public {
        require(hasRole(PAUSER_ROLE, _msgSender()), "BityEGP: must have pauser role to pause");
        _pause();
    }

    // Unpause the contract.
    function unpause() public {
        require(hasRole(PAUSER_ROLE, _msgSender()), "BityEGP: must have pauser role to unpause");
        _unpause();
    } 



     /**
     * AccessControl  
     */

    // Restricted to members of the admin role.
    modifier onlyAdmin() {
        require(isAdmin(msg.sender), "Restricted to admins");
        _;
    }

    // Return `true` if the account belongs to the admin role.
    function isAdmin(address account) public virtual view returns (bool) {
        return hasRole(DEFAULT_ADMIN_ROLE, account);
    }

    // Return `true` if the account belongs to the user role.
    function isUser(address account) public virtual view returns (bool) {
        return hasRole(USER_ROLE, account);
    }

    // Add an account to the user role. Restricted to admins.
    function addUser(address account) public virtual onlyAdmin {
        grantRole(USER_ROLE, account);
    }

    // Add an account to the admin role. Restricted to admins.
    function addAdmin(address account) public virtual onlyAdmin {
        grantRole(DEFAULT_ADMIN_ROLE, account);
    }

    // Remove an account from the user role. Restricted to admins.
    function removeUser(address account) public virtual onlyAdmin {
        revokeRole(USER_ROLE, account);
    }

    // Remove oneself from the admin role.
    function renounceAdmin() public virtual {
        renounceRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }



    /**
     * Hooks  
     */

    // Validation function called before any transaction.
    function _beforeTokenTransfer(address operator, address from, address to, uint256 tokenId) internal virtual override (ERC777UpgradeSafe) {
        super._beforeTokenTransfer(operator, from, to, tokenId);

        require(!paused(), "BityEGP: token transfer while paused");
        require(isUser(to), "BityEGP: Unauthorized 'to' user account!");
        require(isUser(from), "BityEGP: Unauthorized 'from' user account!");
        require(isOperatorFor(msg.sender, from), "BityEGP: Unauthorized token operator!");
    
    }


    // Return the contract version.
    function version() public pure returns (string memory) {
        return "v1";
    }


    uint256[50] private __gap;
}
