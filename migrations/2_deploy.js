
// Load zos scripts and truffle wrapper function
const { scripts, ConfigManager } = require('@openzeppelin/cli');
const { add, push, create } = scripts;

 var defaultOperators = [
        "0x5AB46047789B76800D6501EEDFb0a1e8C5CC1dfF",
        "0xc58F70fC1dFDb5e84FB74ab209640A739Db09bEC",
        "0xAd7B61139c5BB7a74bc4A61876C20909d095B224"
    ];

async function deployBityEGP(options) {
  // Register v1 of the contract in the zos project
  add({ contractsData: [{ name: 'BityEGP', alias: 'BityEGP' }] });
  // Push implementation contracts to the network
  await push(options);
  // Create an instance of the contract
  await create(Object.assign({ 
      contractAlias: 'BityEGP',
      methodName: 'initialize', 
      methodArgs: [defaultOperators] }, options));
}


module.exports = function(deployer, networkName, accounts) {
  deployer.then(async () => {
    const { network, txParams } = await ConfigManager.initNetworkConfiguration({ network: networkName, from: accounts[0] })
    await deployBityEGP({ network, txParams })
  })
}



